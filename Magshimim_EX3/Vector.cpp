#include "Vector.h"
#include<algorithm>
#include <string> 

Vector::Vector(int n)
{
	n = std::max(MINIMUM_CAPACITY, n);
	this->_elements = new int[n]; // allocates dynamic memory
	this->_capacity = n;
	this->_resizeFactor = n;
	this->_size = 0;
}

Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}

int Vector::size() const { return this->_size; } //geters: 

bool Vector::empty() const { return this->_size == 0; }

int Vector::capacity() const { return this->_capacity; }

int Vector::resizeFactor() const { return this->_resizeFactor; }//^^^


void Vector::push_back(const int &val)
{
	if (this->_size == this->_capacity)
	{
		this->_capacity += this->_resizeFactor;
		int * tmp = new int[this->_capacity];

		for (int i = 0; i < this->_size; i++)
		{
			tmp[i] = this->_elements[i];
		}
		delete[] this->_elements;
		this->_elements = tmp;
	}

	this->_elements[this->_size] = val;
	this->_size++;
}

int Vector::pop_back()
{
	if (this->_size == EMPTY)
	{
		std::cerr << "error: pop from empty vector";
			return ERROR;
	}

	this->_size -= 1;
	return this->_elements[this->_size];
}

void Vector::reserve(const int n)
{
	if (n <= this->_capacity)
	{
		return;//does nothing
	}

	int new_capacity = round_up_to_resizeFactor_multuplies(n, this->_resizeFactor);
	int * temp = new int[new_capacity];

	for (int i = 0; i < this->_size; i++)//copy all values:
	{
		temp[i] = this->_elements[i];
	}//^^^

	delete[] this->_elements;
	this->_elements = temp;

	this->_capacity = new_capacity;
}

Vector& Vector::operator+=(const Vector& other)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = this->_elements[i] + other._elements[i];
	}
	return *this;
}

Vector& Vector::operator-=(const Vector& other)
{
	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = this->_elements[i] - other._elements[i];
	}
	return *this;
}

Vector Vector::operator+(const Vector& other) const
{
	int i = 0;
	Vector v3(std::max(this->_size, other._size));

	for (i = 0; i < std::min(this->_size, other._size); i++)
	{
		v3._elements[i] = this->_elements[i] + other._elements[i];
	}

	if (this->_size > other._size)
	{
		for (i = 0; i < this->_size; i++)
		{
			v3._elements[i] = this->_elements[i];
		}
	}
	else
	{
		for (; i < other._size; i++)
		{
			v3._elements[i] = other._elements[i];
		}
	}
	v3._size = std::max(this->_size, other._size);
	return v3;
}

Vector Vector::operator-(const Vector& other) const
{
	int i = 0;
	Vector v3(std::max(this->_size, other._size));

	for (i = 0; i < std::min(this->_size, other._size); i++)
	{
		v3._elements[i] = this->_elements[i] - other._elements[i];
	}

	if (this->_size > other._size)
	{
		for (i = 0; i < this->_size; i++)
		{
			v3._elements[i] = this->_elements[i];
		}
	}
	else
	{
		for (; i < other._size; i++)
		{
			v3._elements[i] = other._elements[i];
		}
	}
	v3._size = std::max(this->_size, other._size);
	return v3;
}

/*
Calc the round of the number to the nearest biger multipication of resize factr and return it.
*/
int Vector::round_up_to_resizeFactor_multuplies(int num, const int & resize_factor)
{
	if (num % resize_factor > 0)
	{
		return num - (num % resize_factor) + resize_factor;
	}
	return num;
}

void Vector::resize(const int n)
{
	resize(n, 0);
}

void Vector::assign(const int val)
{
	for (int i = 0; i < this->_size; i++)//fill all remains with val:
	{
		this->_elements[i] = val;
	}//^^^
}

void Vector::resize(int n, const int& val)
{
	if (n > this->_capacity)
	{
		this->reserve(n);
	}

	for (; this->_size < n; this->_size++)//fill all remains with val:
	{
		this->_elements[this->_size] = val;
	}//^^^

	this->_size = n;
}

Vector::Vector(const Vector & other)
{
	*this = other;
}

Vector & Vector::operator=(const Vector & other)
{
	this->_capacity = other._capacity;
	this->_size = other._size;
	this->_resizeFactor = other._resizeFactor;

	this->_elements = new int[this->_capacity];

	for (int i = 0; i < this->_size; i++)
	{
		this->_elements[i] = other._elements[i];
	}
	return *this;
}

int & Vector::operator[](int n) const
{
	if (n > this->_size - 1 || n < 0)
	{
		n = 0;
	}
	return this->_elements[n];
}

std::ostream& operator<<(std::ostream& os, const Vector& v)
{
	std::string str= "Vector Info:\nCapacity is: " + std::to_string(v._capacity)  + "\nSize is: " + std::to_string(v._size) + "\ndata is: ";

	for (int i = 0; i < v._size; i++)
	{
		str += std::to_string(v._elements[i]) + ", ";
	}
	str += "\n";
	os.write(str.c_str(), str.size());

	return os;
}
